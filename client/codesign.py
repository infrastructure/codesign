#!/usr/bin/env python3

# Copyright (c) 2023 Blender Foundation
#
# SPDX-License-Identifier: MIT-0

# A code-sign client which calculates digest locally, offloads its signing to a
# remote server, and creates file signature and timestamp on the local file once
# the server has provided signed digest.
#
# Overview of the steps:
#
#   1. Create the digest:
#      signtool.exe sign /f .\key\certificate.crt /fd sha256 `
#          /dg .\digest\ blender.exe
#
#   2. Request server to sign .\digest\blender.exe.dig and save the result as
#      .\digest\blender.exe.dig.signed
#
#   3. Create signature on the client:
#      signtool.exe sign /di .\digest\ blender.exe
#
#   4. Add a timestamp:
#      signtool.exe timestamp /tr http://ts.ssl.com /td sha256 blender.exe
#
# This tool requires the signtool.exe tool to be in the current PATH.
#
# The tool is designed to be stand-alone with minimal dependencies and no
# complicated deployment process required. It should be enough to have a
# bare Python install for it to work (surely, the digest signing server needs
# to be configured and available).

import argparse
import json
import os
import shutil
import subprocess
import sys
import tempfile
import time
import urllib.parse

from pathlib import Path
from shutil import which
from typing import Iterable, Optional
from urllib.request import Request, urlopen
from urllib.error import URLError

SIGNTOOL_EXE = "signtool.exe"


def _append_path_suffix(path: Path, suffix: str) -> Path:
    """
    Append suffix to the path in a portable way

    On some platforms like Windows the trailing space on a file path seems
    to be meaningless. For example, both Path("C:\\Windows").exists() and
    Path("C:\\Windows     ").exists() are True.

    This functin takes care of appending suffix in a way that strips the
    trailing spaces of the input file when needed.

    For example _append_path_suffix(Path("C:\\file.exe    ", ".dig"))
    returns Path("C:\\file.exe.dig")
    """

    name = path.name

    if os.name == "nt":
        name = name.rstrip()

    return path.parent / (name + suffix)


def _create_argument_parser() -> argparse.ArgumentParser:
    """
    Create parser of command line arguments

    The parser will be configured to recognize all command line arguments, but
    no actual parsing will happen in this function.
    """

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--server-url",
        type=str,
        help="URL of the server which signs digest hashes. For example http://192.168.0.1:8080",
        required=True,
    )

    parser.add_argument(
        "--description",
        type=str,
        help="Description of the signed content",
        required=False,
    )

    parser.add_argument(
        "files", type=Path, nargs="+", help="List of files to be signed"
    )

    return parser


def _check_command_available(command: str) -> bool:
    """
    Check whether the given command is available and can be called

    Returns true if the command is available, false otherwise
    """

    return which(command) is not None


def _check_setup_or_exit() -> None:
    """
    Check the local setup and exit with non-zero code if something is missing
    """

    if _check_command_available(SIGNTOOL_EXE):
        return

    print(f"ERROR: Command {SIGNTOOL_EXE} is not found", file=sys.stderr)
    sys.exit(1)


def _server_call(server_url: str, function: str, arguments=None) -> dict:
    """
    Perform a REST API call to the digest signing server

    The server_url is expected to contain host and port, for example
    http://192.168.0.1:8080


    The function can be one of following:

    - certificate : retrieve the public certificate from the signing server.
      The function takes no arguments, returns an object like
      {"certificate": "<certificate>"}.

    - sign : sign a digest provided in base64 encoding as "digest" argument.
      The result is base64-encoded and looks like
      {"digest_signed": "<base64-encoded signed digest>"}
    """

    if server_url.endswith("/"):
        url = f"{server_url}{function}"
    else:
        url = f"{server_url}/{function}"

    if arguments:
        params = urllib.parse.urlencode(arguments)
        url += f"?{params}"

    request = Request(url)

    try:
        response = urlopen(request)
    except URLError as exception:
        # TODO(sergey): Find a more type-safe way to access the code.
        if hasattr(exception, "code") and exception.code in (404,):
            body = json.loads(exception.read().decode("utf-8"))
            print(f"ERROR: Server: {body['error']}", file=sys.stderr)
        else:
            print(
                f"ERROR: Error while communicating to server: {exception.reason}",
                file=sys.stderr,
            )

        sys.exit(1)

    return json.loads(response.read().decode("utf-8"))


class CodeSignPipeline:
    """
    Class which implements code-signing pipeline of a multiple files
    """

    # URL of the server which takes care of signing digests.
    # For example, http://192.168.0.1:8080
    _server_url: str

    # Description of the signed content
    _description: Optional[str]

    # Temporary storage directory which is used to store both state which is
    # global for all the files which are to be signed and for the individual
    # files code-signing.
    # This storage is deleted even if an unexpected error occurs during the code
    # signing process.
    _storage_dir_handle: tempfile.TemporaryDirectory
    _storage_dir: Path

    # Path to a public certificate file.
    # It is used for code-sining all requested files.
    _key_file: Path

    # Directory where code-signing digests are stored.
    # The directory itself is re-used by different code-signing files, its
    # content is cleared before new file is signed.
    _digest_dir: Path

    def __init__(self, server_url: str, description: Optional[str]) -> None:
        """
        Initialize invariant part of the code-signing process
        """

        self._server_url = server_url
        self._description = description

        self._storage_dir_handle = tempfile.TemporaryDirectory()
        self._storage_dir = Path(self._storage_dir_handle.name)

        self._key_file = self._retrieve_certificate()

        self._digest_dir = self._storage_dir / "digest"

    def _retrieve_certificate(self) -> Path:
        """
        Retrieve the code-signing public certificate from the server

        Returns path to the file where it was saved locally.
        """

        print("==> Requesting public certificate for code signing")
        response = _server_call(self._server_url, "certificate")

        print("Received the public certificate")

        file_path = self._storage_dir / "key" / "certificate.crt"
        file_path.parent.mkdir()

        file_path.write_text(response["certificate"])

        return file_path

    def sign_files(self, file_paths: Iterable[Path]) -> None:
        """
        Sign the given list of files

        The files are signed in-place.
        """

        # Code-sign individual files.
        for file_path in file_paths:
            self._sign_single_file(file_path)

        # Timestamp all files at once, to save communication with an external
        # server which is potentially much slower than communication with the
        # local signing server.
        self._timestamp_all(file_paths)

    def _sign_single_file(self, file_path: Path) -> None:
        """
        Sign the single file

        The file is signed in-place.
        """

        print(f"==> Handling `{file_path}`")

        if not file_path.exists():
            print(
                f"ERROR: Requested file `{file_path}` does not exist",
                file=sys.stderr,
            )
            sys.exit(1)
        if not file_path.is_file():
            print(
                f"ERROR: Requested file `{file_path}` is not a file",
                file=sys.stderr,
            )
            sys.exit(1)

        self._remove_signature(file_path)
        self._create_digest(file_path)
        self._sign_digest(file_path)
        self._create_signature(file_path)

    def _remove_signature(self, file_path: Path) -> None:
        """
        Remove signature from the given file

        Command line example:
                signtool.exe remove "/s" blender.exe
        """

        print("--> Ensure the original file is not signed")
        print(
            "    If the file is not signed yet the CryptSIPRemoveSignedDataMsg "
            "will be reported below"
        )

        # Do not check the error code because removal of signature from a
        # non-signed file might be considered as an error.
        self._signtool(("remove", "/s", file_path), check=False)

    def _create_digest(self, file_path: Path) -> None:
        r"""
        Create digest of the file to be signed

        Command line example:
                signtool.exe sign /f .\key\certificate.crt `
                        /fd sha256 /dg .\digest\ blender.exe
        """

        print("--> Creating digest")

        # Make sure the digest directory exists and is empty.
        if self._digest_dir.exists():
            shutil.rmtree(self._digest_dir)
        self._digest_dir.mkdir()

        command = ["sign", "/f", self._key_file, "/fd", "sha256"]
        if self._description:
            command += ["/d", self._description]
        command += ["/dg", self._digest_dir, file_path]

        self._signtool(command)

    def _sign_digest(self, file_path: Path) -> None:
        """
        Sign the digest of the given file on the server
        """

        print("--> Signing digest")

        digest_file = self._get_digest_file(file_path)
        assert digest_file.exists()

        digest_base64 = digest_file.read_text()

        response = _server_call(
            self._server_url, "sign", {"digest": digest_base64}
        )

        digest_signed_file = self._get_signed_digest_file(file_path)
        digest_signed_file.write_text(response["digest_signed"])

    def _create_signature(self, file_path: Path) -> None:
        r"""
        Create the signature in the given file

        Basically, applies the signed digest into the file.

        Expects that the digest directory contains digest and signed digest for
        the given file.

        Command line example:
            signtool.exe sign /di .\digest\ blender.exe
        """

        print("--> Creating signature")
        command = ["sign"]
        if self._description:
            command += ["/d", self._description]
        command += ["/di", self._digest_dir, file_path]
        self._signtool(command)

    def _timestamp_all(self, file_paths: Iterable[Path]) -> None:
        """
        Timestamp all files from the file_paths

        Command line example:
            signtool.exe timestamp /tr http://ts.ssl.com /td sha256 blender.exe
        """

        print("==> Timestamp all given files")

        # TODO(sergey): Consider making timestamp server configurable.
        self._signtool(
            (
                "timestamp",
                "/tr",
                "http://ts.ssl.com",
                "/td",
                "sha256",
                *file_paths,
            )
        )

    def _get_digest_file(self, file_path: Path) -> Path:
        """
        Get path to a digest file which corresponds to the file_path
        """

        return self._digest_dir / _append_path_suffix(file_path, ".dig").name

    def _get_signed_digest_file(self, file_path: Path) -> Path:
        """
        Get path to a signed digest file which corresponds to the file_path
        """

        digest_file = self._get_digest_file(file_path)
        return _append_path_suffix(digest_file, ".signed")

    def _signtool_filter_command(self, arguments: Iterable[str]) -> list[str]:
        """
        Filter signtool command arguments suitable for logging

        Strips all information which is not desirable to end up in the public
        log, or information which is simply too noisy and not useful.
        """

        filtered = []

        for arg in arguments:
            if isinstance(arg, Path):
                # Make path relative to the storage directory, hiding the
                # verbose prefix to the temp folder.
                if arg.is_relative_to(self._storage_dir):
                    filtered.append(str(arg.relative_to(self._storage_dir)))
                    continue

            filtered.append(str(arg))

        return filtered

    def _signtool(self, arguments: Iterable[str], check=True) -> None:
        """
        A wrapper around signtool.exe

        Logs redacted command and executes the signtool with the given
        arguments.
        """

        command = [SIGNTOOL_EXE] + list(arguments)
        print(
            f"    Running: {' '.join(self._signtool_filter_command(command))}"
        )
        subprocess.run(command, check=check)


def main() -> None:
    """
    Main application entry point
    """

    start_time = time.perf_counter()

    _check_setup_or_exit()

    parser = _create_argument_parser()
    args = parser.parse_args()

    pipeline = CodeSignPipeline(server_url=args.server_url,
                                description=args.description)
    pipeline.sign_files(args.files)

    end_time = time.perf_counter()
    elapsed_time = end_time - start_time
    # print("Elapsed time: ", elapsed_time)

    # input("Press Enter to continue...")


if __name__ == "__main__":
    main()
