# Copyright (c) 2023 Blender Foundation
#
# SPDX-License-Identifier: MIT-0

# Utilities which are used for an early environment check and reporting of a
# missing bits with hints about how to correct them.

import sys

from shutil import which

from logger import logger
from config.config import Config

# Commands which are expected to present in the environment and a brief
# explanation how to get them on a Debian/Ubuntu-like Linux distro.
_REQUIRED_COMMANDS = (
    ("ykman", "Use `sudo apt install yubikey-manager`"),
    ("openssl", "Use `sudo apt install openssl`"),
)


def _check_command_available(command: str) -> bool:
    """
    Check whether the given command is available and can be called

    Returns true if the command is available, false otherwise
    """

    return which(command) is not None


def _check_common_command_or_exit(command: str, details: str) -> None:
    """
    Check the command exists

    If the command does not exist print an error message with a hint about how
    to obtain the command, and the application exists with a non-zero code.
    """

    if _check_command_available(command):
        logger.info("Found command `%s`", command)
        return

    print(f"ERROR: Command `{command}` not found. {details}", file=sys.stderr)
    sys.exit(1)


def check_or_exit() -> None:
    """
    Check whether the environment is suitable for the server operation

    If a missing component is detected a hint about it is reported and the
    application exists with a non-zero code.
    """

    config = Config.get()

    for command in _REQUIRED_COMMANDS:
        _check_common_command_or_exit(command[0], command[1])

    # TODO(sergey): Check Yubikey slot

    if config.PKCS11_MODULE_PATH.exists():
        logger.info(f"Found PKCS11 module: {config.PKCS11_MODULE_PATH}")
    else:
        print(
            f"PKCS11 module {config.PKCS11_MODULE_PATH} is not found",
            file=sys.stderr,
        )
        sys.exit(1)
