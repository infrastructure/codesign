# Copyright (c) 2023 Blender Foundation
#
# SPDX-License-Identifier: MIT-0

# Common logger object and utilities.

import logging

logger = logging.getLogger("codesign").getChild("server")
