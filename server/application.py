# Copyright (c) 2023 Blender Foundation
#
# SPDX-License-Identifier: MIT-0

import asyncio
import logging
import logging.config
import tornado

import check_setup

from logger import logger
from config.config import Config

from handler.certificate import CertificateHandler
from handler.home import HomePageHandler
from handler.sign import SignHandler


def _create_web_application() -> tornado.web.Application:
    """
    Create web application with the rotes configured
    """

    return tornado.web.Application(
        [
            (r"/", HomePageHandler),
            (r"/certificate", CertificateHandler),
            (r"/sign", SignHandler),
        ]
    )


class Application:
    """
    The Application class

    Contains the main business logic.
    """

    _bind_address: str
    _bind_port: int

    _web_app: tornado.web.Application

    def __init__(self) -> None:
        config = Config.get()

        logging.config.dictConfig(config.LOGGING)

        check_setup.check_or_exit()

        self._bind_address = config.BIND_ADDRESS
        self._bind_port = config.BIND_PORT

        self._web_app = _create_web_application()

    def run(self) -> None:
        """
        Run the code signing application for a very long time
        """

        asyncio.run(self._serve())

    async def _serve(self) -> None:
        """
        The asynchronous server entry point
        """

        logger.info(
            "Started new digest codesign server on %s:%d",
            self._bind_address,
            self._bind_port,
        )

        self._web_app.listen(address=self._bind_address, port=self._bind_port)
        await asyncio.Event().wait()
