# Copyright (c) 2023 Blender Foundation
#
# SPDX-License-Identifier: MIT-0

# Handler of the public certificate request.

import subprocess
import tornado.web

from typing import Iterable, Tuple

from config.config import Config
from logger import logger


def _get_ykman_version() -> Iterable[int]:
    """
    Get current ykman command version as a tuple of integers

    The major version is the integer at index 0 of the return tuple.
    """

    output = subprocess.check_output(
        ("ykman", "--version"), stderr=subprocess.STDOUT
    )

    # Example of the command output:
    #
    # YubiKey Manager (ykman) version: 3.1.1
    # Libraries:
    #     libykpers 1.20.0
    #     libusb 1.0.23

    lines = output.decode().splitlines()

    version_str = lines[0].split(":", 1)[1].strip()

    return [int(x) for x in version_str.split(".")]


def _get_export_command() -> Tuple[str, ...]:
    """
    Get command for the public certificate export
    """

    config = Config.get()
    slot = config.YUBIKEY_SLOT

    version = _get_ykman_version()

    if version[0] < 4:
        return ("ykman", "piv", "export-certificate", slot, "-")

    return ("ykman", "piv", "certificates", "export", slot, "-")


class CertificateHandler(tornado.web.RequestHandler):
    """
    Handler of the public certificate request.
    """

    def get(self):
        "Handler itself"

        logger.info(
            "Request public certificate from %s", self.request.remote_ip
        )

        command = _get_export_command()

        try:
            certificate = subprocess.check_output(
                command, stderr=subprocess.STDOUT
            )
        except subprocess.CalledProcessError as exception:
            logger.error(
                "Error exporting certificate: %s",
                exception.output.decode().strip(),
            )

            self.set_status(404)
            self.write({"error": "Unable to access public certificate"})

            return

        self.write({"certificate": certificate.decode()})
