# Copyright (c) 2023 Blender Foundation
#
# SPDX-License-Identifier: MIT-0

import tornado


class HomePageHandler(tornado.web.RequestHandler):
    """
    Handler of the home page of the server

    Mainly used to see whether the server is up.
    """

    def get(self):
        "Handler itself"

        self.write("Server OK")
