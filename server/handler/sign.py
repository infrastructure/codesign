# Copyright (c) 2023 Blender Foundation
#
# SPDX-License-Identifier: MIT-0

# Handler of the sign command.
#
# Expects the digest to be provided in a base64 encoding. Performs signing and
# returns result as base64-encoded string.
#
# This is similar to manually calling the following command:

#     cat blender.exe.dig | base64 -d | \
#       openssl pkeyutl \
#         -engine pkcs11 -keyform engine -inkey "pkcs11:object=Private key for PIV Authentication;type=private;pin-value=123456" \
#         -sign -pkeyopt digest:sha256 | \
#       base64 > blender.exe.dig.signed


import base64
import os
import subprocess
import tornado.web

from typing import Optional, Tuple

from config.config import Config
from logger import logger

# Mapping from Yubikey slot name to key object name understandable by the
# OpenSSL.
#
# https://developers.yubico.com/yubico-piv-tool/YKCS11/Functions_and_values.html
_SLOT_TO_OBJECT = {
    "9a": "Private key for PIV Authentication",
    "9c": "Private key for Digital Signature",
    "9d": "Private key for Key Management",
}


def _get_key_string() -> str:
    """
    Get key in a format expected by the OpenSSL.

    Addresses the configured slot and provides pin.
    """

    config = Config.get()
    object_name = _SLOT_TO_OBJECT[config.YUBIKEY_SLOT]

    return (
        f"pkcs11:object={object_name};type=private;pin-value={config.PIV_PIN}"
    )


def _get_openssl_command() -> Tuple[str, ...]:
    """
    Construct OpenSSL command.
    """

    key_str = _get_key_string()

    return (
        "openssl",
        "pkeyutl",
        "-engine",
        "pkcs11",
        "-keyform",
        "engine",
        "-inkey",
        key_str,
        "-sign",
        "-pkeyopt",
        "digest:sha256",
    )


def _get_openssl_environment() -> dict:
    """
    Get environment which is passed to the OpenSSL command

    Contains configuration needed the OpenSSL to talk to the Yubikey's PKCS11
    implementation.
    """

    config = Config.get()

    new_env = os.environ.copy()
    new_env["PKCS11_MODULE_PATH"] = config.PKCS11_MODULE_PATH

    return new_env


def _sign_digest(digest: str) -> str:
    """
    Sign the digest and return the signed return

    The digest is expected to be base64-encoded, the result is base64-encoded
    as well.
    """

    digest_bin = base64.b64decode(digest)

    openssl_command = _get_openssl_command()
    openssl_environment = _get_openssl_environment()

    try:
        signed_bin = subprocess.check_output(
            openssl_command,
            env=openssl_environment,
            input=digest_bin,
            stderr=subprocess.DEVNULL,
        )
    except subprocess.CalledProcessError as exception:
        return None

    return base64.b64encode(signed_bin).decode()


class SignHandler(tornado.web.RequestHandler):
    """
    Handler of the /sign command.
    """

    def get(self):
        "Handler itself"

        digest = self.get_argument("digest", None)
        if digest is None:
            self.set_status(404)
            self.write({"error": "Missing argument `digest`"})
            return

        logger.info(
            "Request to sign digest %s from %s", digest, self.request.remote_ip
        )

        digest_signed = _sign_digest(digest)
        if digest_signed is None:
            self.set_status(404)
            self.write({"error": "Error signing digest"})
            return

        self.write({"digest_signed": digest_signed})
