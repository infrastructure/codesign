#!/usr/bin/env python3

# Copyright (c) 2023 Blender Foundation
#
# SPDX-License-Identifier: MIT-0

from application import Application

from config.config import Config
from config.config_prod import ProdConfig


def main() -> None:
    """
    Main application entry point
    """

    Config.set(ProdConfig())

    app = Application()
    app.run()


if __name__ == "__main__":
    main()
