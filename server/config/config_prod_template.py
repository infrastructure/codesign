# Copyright (c) 2023 Blender Foundation
#
# SPDX-License-Identifier: MIT-0

# A template of the production configuration.
#
# Acts as an example of how the default configuration can be overwritten.

from config.config import Config


class ProdConfig(Config):
    """
    Production configuration
    """

    def __init__(self) -> None:
        super().__init__()

        self.PIV_PIN = "123456"
