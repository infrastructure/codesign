# Copyright (c) 2023 Blender Foundation
#
# SPDX-License-Identifier: MIT-0

# Base class of the configuration.
#
# Contains default values, which act as an example values.
# Owerride in a sub-class, i.e. in ProdConfig.

from pathlib import Path


class Config:
    """
    Code-signing application configuration.
    """

    _instance: "Config"

    def __init__(self):
        # Address and port the signing server is listening on.
        self.BIND_ADDRESS = "0.0.0.0"
        self.BIND_PORT = 8080

        # Yubikey slot at which the code sign certificate is stored.
        self.YUBIKEY_SLOT = "9a"

        self.PKCS11_MODULE_PATH = Path(
            "/usr/lib/x86_64-linux-gnu/libykcs11.so.2"
        )

        # PIN code which is required to perform signing operation.
        self.PIV_PIN = "123456"

        # https://docs.python.org/3/library/logging.config.html#configuration-dictionary-schema
        self.LOGGING = {
            "version": 1,
            "formatters": {
                "default": {
                    "format": "%(asctime)-15s %(levelname)8s %(name)s %(message)s"
                }
            },
            "handlers": {
                "console": {
                    "class": "logging.StreamHandler",
                    "formatter": "default",
                    "stream": "ext://sys.stderr",
                }
            },
            "loggers": {
                "codesign": {"level": "INFO"},
            },
            "root": {
                "level": "WARNING",
                "handlers": [
                    "console",
                ],
            },
        }

    @classmethod
    def set(cls, config: "Config") -> None:
        """
        Set an actual configuration instance.
        """

        cls._instance = config

    @classmethod
    def get(cls) -> "Config":
        """
        Get the current configuration instance.
        """

        return cls._instance
